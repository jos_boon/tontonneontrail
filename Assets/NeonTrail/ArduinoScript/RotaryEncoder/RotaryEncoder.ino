#define playerOneA 2
#define playerOneB 3
#define playerOneButton 4

#define playerTwoA 5
#define playerTwoB 6
#define playerTwoButton 7

#define playerThreeA 8
#define playerThreeB 9
#define playerThreeButton 10

#define playerFourA 11
#define playerFourB 12
#define playerFourButton 13

#define PlayerCount 4

struct DialPlayer
{
  int pinA;
  int pinB;
  int pinButton;

  int aPrevState;
  int bPrevState;
  int buttonState;

  DialPlayer() {}
  
  DialPlayer(int a, int b, int c)
  {
    pinA = a;
    pinB = b;
    pinButton = c;
  }
};

DialPlayer players[4] = { };

void setup() 
{
  players[0] = DialPlayer(playerOneA, playerOneB, playerOneButton);
  players[1] = DialPlayer(playerTwoA, playerTwoB, playerTwoButton);
  players[2] = DialPlayer(playerThreeA, playerThreeB, playerThreeButton);
  players[3] = DialPlayer(playerFourA, playerFourB, playerFourButton);

  Serial.begin(115200);

  init();
}

void init()
{
  for(int i = 0; i < PlayerCount; i++)
  {
    players[i].aPrevState = digitalRead(players[i].pinA);
    players[i].bPrevState = digitalRead(players[i].pinB);
    players[i].buttonState = digitalRead(players[i].pinButton);
  }
}

void loop() 
{
  checkInput();
}

void checkInput()
{
  String s = "";
  for(int i = 0; i < PlayerCount; i++)
  {
    int rotation;
    int buttonPress;
    
    playerInput(players[i], rotation, buttonPress);

    if(rotation == 0 && buttonPress == 0)
      continue;
      
    s += ";";
    s += i;
    s += ",";
    s += rotation;
    s += ",";
    s += buttonPress;
    s += ";";     
  }

  // Input that has been registered will be sent
  if(s.length() > 0)
  {
    Serial.println(s);
    Serial.flush();
  }
}

void playerInput(DialPlayer &player, int &rotation, int &buttonPress)
{
  rotation = 0;
  buttonPress = 0;

  int currentStateA = digitalRead(player.pinA);
  int currentButtonState = digitalRead(player.pinButton);

  if(currentStateA != player.aPrevState)
  {
    if(digitalRead(player.pinB) != currentStateA)
      rotation = -1;
    else
      rotation = 1;

    player.aPrevState = currentStateA;
  }

  if(currentButtonState == 0)
    buttonPress = 1;
}
