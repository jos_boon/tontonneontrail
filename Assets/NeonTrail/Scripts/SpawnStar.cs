﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnStar : MonoBehaviour
{
    public Star[] stars;

    public Transform Spawn(string playerName)
    {
        foreach(Star star in stars)
        {
            if (star.name == playerName)
                return Instantiate(star.prefabStar, star.spawnPoint.position, star.spawnPoint.rotation, transform);
        }

        return null;
    }

    [System.Serializable]
    public struct Star
    {
        public string name;
        public Transform prefabStar;
        public Transform spawnPoint;
    }

}
