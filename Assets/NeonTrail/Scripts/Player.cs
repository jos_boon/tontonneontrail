﻿using UnityEngine;

// TODO: Consider Scriptable objects that holds data for ships (Colour, sprite animations, ect) that can be fed
// into the Ship behaviour script. This will remove the need to have multiple prefab variants and might also be easier for setting up new player object or having more than four colours.
[System.Serializable]
public class Player
{
    [SerializeField]
    string _name = "Player name";
    [SerializeField]
    PlayerCorner _playerCorner = PlayerCorner.None;
    [SerializeField]
    Material _fontMaterial = null;

    public string Name => _name;
    public int Wins { get; set; } = 0;
    public bool IsActive { get; set; } = false;
    public PlayerCorner playerCorner => _playerCorner;
    public CrossInput InputDevice { get; set; } = null;
    public ControllableObject ControllableObject => InputDevice.ControllableObject;
    public Material FontMaterial => _fontMaterial;

    public void Reset()
    {
        InputDevice = null;
        IsActive = false;
        Wins = 0;
    }

    public enum PlayerCorner
    {
        TopRight,
        TopLeft,
        BottomRight,
        BottomLeft,
        None
    }
}
