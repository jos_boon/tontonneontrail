﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TogglePlayerScore : MonoBehaviour
{
    [SerializeField]
    Player.PlayerCorner corner = Player.PlayerCorner.None;
    
    Image _score;

    void Awake()
    {
        _score = GetComponent<Image>();

        if (GameManager.Instance)
            GameManager.Instance.OnGameStateChange += ShowScore;
    }

    void OnDestroy()
    {
        if (GameManager.Instance)
            GameManager.Instance.OnGameStateChange -= ShowScore;
    }

    void ShowScore(GameInfo gameInfo)
    {
        if (gameInfo.GameState == GameState.StartRound)
            _score.enabled = false;            

        if (gameInfo.GameState != GameState.EndofRound && gameInfo.GameState != GameState.GameOver)
            return;

        var player = gameInfo.JoinedPlayers.FirstOrDefault(p => p.playerCorner == corner);
        if (player != null)
            _score.enabled = true;

    }
}
