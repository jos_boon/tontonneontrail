﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimator : MonoBehaviour
{
    public delegate void OnFrameEvent();
    public event OnFrameEvent OnAnimationFinishedEvent;

    public SpriteRenderer spriteRenderer;
    public float framesPerSecond = 1.0f;

    [SerializeField]
    Sprite[] frames = null;
    [SerializeField]
    Sprite endFrame = null;
    [SerializeField]
    bool startOnAwake = false;
    [SerializeField]
    bool loop = false;

    bool running = false;
    int index = 0;
    float timer = 0;
    float nextFrameDuration = 0;

    void Awake()
    {
        if (startOnAwake)
            running = true;

        if(!spriteRenderer)
        {
            running = false;
            enabled = false;
            Debug.Log($"{gameObject.name} - SpriteAnimator: Missing SpriteRenderer!");
        }

        nextFrameDuration = 1.0f / framesPerSecond;
    }

    void Update()
    {
        if (!running || frames.Length == 0)
            return;

        timer += Time.deltaTime;
        if(timer > nextFrameDuration)
        {
            timer = 0;
            ++index;

            if (loop)
                index %= frames.Length;

            if (index >= frames.Length)
            {
                if(endFrame)
                    spriteRenderer.sprite = endFrame;

                running = false;

                if (OnAnimationFinishedEvent != null)
                    OnAnimationFinishedEvent.Invoke();
            }     
            else
                spriteRenderer.sprite = frames[index]; 
        }
            
    }

    public void Play()
    {
        running = true;
        timer = 0;
        index = 0;
        spriteRenderer.sprite = frames[index];
    }

    public void Stop(bool unsetFrame = false)
    {
        running = false;

        if (unsetFrame)
            spriteRenderer.sprite = null;
    }


}
