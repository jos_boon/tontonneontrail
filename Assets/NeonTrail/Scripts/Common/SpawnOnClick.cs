﻿using UnityEngine;

public class SpawnOnClick : MonoBehaviour
{
    [SerializeField]
    Transform prefabObstacle = null;
    [SerializeField]
    string tagName = "Obstacle";

    Camera mainCamera;

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        if (!prefabObstacle)
            return;

        Vector3 screenPosition = Input.mousePosition + new Vector3(0, 0, 10);
        Vector3 worldPosition = mainCamera.ScreenToWorldPoint(screenPosition);

        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(prefabObstacle, worldPosition, Quaternion.identity, transform);
        }
        else if(Input.GetMouseButtonDown(1))
        {
            RaycastHit2D hitinfo = Physics2D.CircleCast(worldPosition, .1f, Vector2.zero);

            if (hitinfo.collider && hitinfo.collider.CompareTag(tagName))
                Destroy(hitinfo.collider.gameObject);
        }
    }
}
