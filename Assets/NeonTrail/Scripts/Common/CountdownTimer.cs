﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountdownTimer : MonoBehaviour
{
    public delegate void OnCountDownTickEvent(CountdownTimer cdt);
    public delegate void OnCountDownCompleteEvent();

    public event Action<CountdownTimer> OnTickEvent;
    public event Action OnCompleteEvent;

    [SerializeField]
    float _duration = 5.0f;

    public float Duration 
    { 
        get => _duration; 
        set => _duration = value; 
    }
    public float Timer { get; private set; } = 0.0f;
    public bool IsRunning { get; private set; } = false;

    void Start()
    {
        Timer = Duration;
    }

    void Update()
    {
        if (!IsRunning)
            return;

        Timer -= Time.deltaTime;

        if (OnTickEvent != null)
            OnTickEvent.DynamicInvoke(this);

        if(Timer <= 0)
        {
            Timer = 0;
            IsRunning = false;
            if (OnCompleteEvent != null)
                OnCompleteEvent.Invoke();
        }
    }

    public void ClearEvents()
    {
        OnTickEvent = null;
        OnCompleteEvent = null;
    }

    public void StartTimer(bool reset = false)
    {
        IsRunning = true;

        if (reset)
            ResetTimer();
    }
    public void StopTimer() => IsRunning = false;
    public void ResetTimer() => Timer = Duration;
}
