﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotatable : ControllableObject
{
    [SerializeField]
    float _speed = 2.0f;

    float _zRotation = 0;

    public override void Rotate(float direction)
    {
        _zRotation += _speed * direction * Time.deltaTime;
        transform.localRotation = Quaternion.Euler(0, 0, _zRotation);
    }

    public override void UseAbility()
    {
        
    }
}
