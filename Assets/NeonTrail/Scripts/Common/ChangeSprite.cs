﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeSprite : MonoBehaviour
{
    [SerializeField]
    Sprite[] images = null;
    [SerializeField]
    KeyCode key = KeyCode.Tab;

    int index = 0;
    SpriteRenderer spriteRenderer;

    public int SpriteIndex => index;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void OnDisable()
    {
        if (!ImageManager.Instance)
            return;

        ImageManager.Instance.UpdateImageState(this);
    }

    void Start()
    {
        if (!ImageManager.Instance)
        {
            Debug.Log("ChangeSprite: ImageManager does not exist!");
            return;
        }

        bool isAdded = ImageManager.Instance.AddImageState(this);

        if (!isAdded)
            ImageManager.Instance.SetChangeSpriteIndex(this);
    }

    void Update()
    {
        if(Input.GetKeyDown(key))
            NextSprite();
    }

    public void ChangeSpriteByIndex(int newIndex)
    {
        if (index < 0 || index > images.Length - 1)
            return;

        index = newIndex;
        spriteRenderer.sprite = images[index];
    }

    public void NextSprite()
    {
        if (images.Length < 0)
            return;

        index = ++index % images.Length;
        spriteRenderer.sprite = images[index];
    }

}
