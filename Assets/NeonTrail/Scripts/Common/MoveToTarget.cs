﻿using System;
using System.Collections;
using UnityEngine;

public class MoveToTarget : MonoBehaviour
{
    public event Action OnDestinationReached;

    [SerializeField]
    float _duration = 2.0f;

    Coroutine _moveTo;

    public void SetTarget(Vector3 targetWorldPosition)
    {
        var localTargetPosition = transform.parent?.InverseTransformPoint(targetWorldPosition) ?? targetWorldPosition;

        if (_duration <= 0)
        {
            transform.localPosition = localTargetPosition;
            OnDestinationReached?.Invoke();
            return;
        }

        if (_moveTo != null)
            StopCoroutine(_moveTo);

        _moveTo = StartCoroutine(Move(localTargetPosition));     
    }

    IEnumerator Move(Vector3 endPosition)
    {
        var startPosition = transform.localPosition;
        var startTime = Time.time;
        var currentTime = startTime;

        while (currentTime - startTime < _duration)
        {
            currentTime = Time.time;
            var progress = (currentTime - startTime) / _duration;

            transform.localPosition = Vector3.Lerp(startPosition, endPosition, progress);
            yield return null;
        }

        transform.localPosition = endPosition;

        OnDestinationReached?.Invoke();
    }
}
