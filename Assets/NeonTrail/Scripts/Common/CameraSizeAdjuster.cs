﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
[RequireComponent(typeof(Camera))]
public class CameraSizeAdjuster : MonoBehaviour
{
    [SerializeField]
    float _baseZoom = 10.0f;
    [SerializeField]
    float _baseAspectRatio = 16.0f / 9.0f;

    Camera _camera;
    float _previousAspectRatio = 0.0f;

    void Awake()
    {
        _camera = GetComponent<Camera>();
    }

    void Update()
    {
        if (!_camera.orthographic || _previousAspectRatio == _camera.aspect)
            return;
        
        _camera.orthographicSize = _baseZoom * (_baseAspectRatio / _camera.aspect);
        _previousAspectRatio = _camera.aspect;        
    }
}
