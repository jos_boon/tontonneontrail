﻿using System.Linq;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    SpawnableObject[] _spawnableObjects = null;

    public Transform SpawnObject(string nameOfObject, Transform parent = null)
    {
        var spawnObject = GetSpawnableObject(nameOfObject);
        if(spawnObject.objectToSpawn)
            return Instantiate(spawnObject.objectToSpawn, parent, false);

        return null;
    }

    public Transform SpawnObject(string nameOfObject, Vector3 position, Quaternion rotation, Transform parent = null)
    {
        var spawnObject = GetSpawnableObject(nameOfObject);
        if(spawnObject.objectToSpawn)
            return Instantiate(spawnObject.objectToSpawn, position, rotation, parent);

        return null;
    }

    
    SpawnableObject GetSpawnableObject(string name)
    {
        // Duplicates shouldn't exist in this list
        return _spawnableObjects.SingleOrDefault(x => x.name == name);
    }

    [System.Serializable]
    public struct SpawnableObject
    {
        public string name;
        public Transform objectToSpawn;
    }
}
