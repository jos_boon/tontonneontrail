﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    static T instance;

    public static T Instance => instance;

    /// <summary>
    /// Set the instance with the given argument and if the instance was already set by a different object it will destroy the object that has been sent
    /// </summary>
    /// <param name="objectInstance"></param>
    /// <returns>
    /// Succeeds in setting the Instance or not
    /// </returns>
    protected bool SetInstance(T objectInstance)
    {
        if (objectInstance == null)
            return false;

        if (instance == objectInstance)
            return true;

        if (instance == null)
        {
            instance = objectInstance;
            return true;
        }

        Debug.Log($"Singleton: Duplicate detected! Destroying gameobject {objectInstance.name}!");
        Destroy(objectInstance.gameObject);

        return false;
    }
}
