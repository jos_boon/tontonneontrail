﻿using UnityEngine;

public class ToggleGameobjects : MonoBehaviour
{
    [SerializeField]
    GameObject[] gameobjects = new GameObject[] { };

    public void Toggle()
    {
        foreach(GameObject gObj in gameobjects)
            gObj.SetActive(!gObj.activeSelf);
    }
}
