﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArdityInputReader : Singleton<ArdityInputReader>
{
    [SerializeField]
    SerialController serialController = null;

    [Header("Player Settings")]
    [SerializeField]
    int playerCount = 4;
    [SerializeField]
    char seperatorPlayers = ';';
    [SerializeField]
    char separatorInput = ',';

    PlayerDialState[] playerDialStates;

    public PlayerDialState this [int i]
    {
        get
        {
            if (i < 0 || i > playerCount)
                return null;

            return playerDialStates[i];
        }
    }

    void Awake()
    {
        if (!SetInstance(this))
            return;
    }

    void Start()
    {
        if (!serialController)
            enabled = false;

        playerDialStates = new PlayerDialState[playerCount];

        for (int i = 0; i < playerDialStates.Length; i++)
            playerDialStates[i] = new PlayerDialState();
    }

    void Update()
    {
        // Go through entire queue stack
        for(int i = 0; i < serialController.maxUnreadMessages; i++)
        {
            string message = serialController.ReadSerialMessage();

            if (message == null)
                continue;

            if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_CONNECTED))
                Debug.Log("Connection established");
            else if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_DISCONNECTED))
                Debug.Log("Connection attempt failed or disconnection detected");
            else
                ExtractData(message);
        }
        
    }

    void ExtractData(string message)
    {
        string[] playerData = message.Split(seperatorPlayers);
        
        for(int i = 0; i < playerData.Length; i++)
        {
            string[] playerInput = playerData[i].Split(separatorInput);

            if (playerInput.Length != 3)
                continue;

            int playerIndex;

            if(int.TryParse(playerInput[0], out playerIndex) && playerIndex < playerCount)
            {
                // reset
                if(playerDialStates[playerIndex].consumed)
                {
                    playerDialStates[playerIndex].direction = 0;
                    playerDialStates[playerIndex].buttonPress = false;
                    playerDialStates[playerIndex].consumed = false;
                }

                int direction;
                int buttonPress;

                if (int.TryParse(playerInput[1], out direction))
                    playerDialStates[playerIndex].direction += direction;

                // Don't unset buttonpress if we have registered button press
                if (!playerDialStates[playerIndex].buttonPress && int.TryParse(playerInput[2], out buttonPress))
                    playerDialStates[playerIndex].buttonPress = System.Convert.ToBoolean(buttonPress);

                playerDialStates[playerIndex].consumed = false;
            }

        }
    }

    public class PlayerDialState
    {
        public int direction;
        public bool buttonPress;
        public bool consumed;
    }
}
