﻿using UnityEngine;
using TMPro;

public class PlayerSelectUIManager : MonoBehaviour
{
    TextMeshProUGUI[] _textCountdown;

    void Awake()
    {
        _textCountdown = GetComponentsInChildren<TextMeshProUGUI>(true);
    }

    void OnEnable()
    {
        if (!GameManager.Instance)
            return;

        GameManager.Instance.CountdownTimer.OnTickEvent += UpdateText;
        GameManager.Instance.OnCountdownStart += EnableCountdownTextComponents;
    }

    void OnDisable()
    {
        if (!GameManager.Instance)
            return;

        GameManager.Instance.CountdownTimer.OnTickEvent -= UpdateText;
        GameManager.Instance.OnCountdownStart -= EnableCountdownTextComponents;
    }

    void EnableCountdownTextComponents()
    {
        foreach (var item in _textCountdown)
            item.enabled = true;
    }

    void UpdateText(CountdownTimer countdownTimer)
    {
        string message = countdownTimer.Timer.ToString("0");

        foreach (var item in _textCountdown)
            item.text = message;
    }
}
