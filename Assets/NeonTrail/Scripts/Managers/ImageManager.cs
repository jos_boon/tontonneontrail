﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageManager : Singleton<ImageManager>
{
    List<ImageIndexState> gameobjectIndexState;

    void Awake()
    {
        if (!SetInstance(this))
            return;

        gameobjectIndexState = new List<ImageIndexState>();

        DontDestroyOnLoad(this);
    }

    public bool AddImageState(ChangeSprite changeableSprite)
    {
        if (!gameobjectIndexState.Exists(x => x.name == changeableSprite.name))
        {
            gameobjectIndexState.Add(new ImageIndexState() { index = changeableSprite.SpriteIndex, name = changeableSprite.name });
            return true;
        }
        else
            Debug.Log($"ChangeSprite object already exist with this name: {changeableSprite.name}");

        return false;
    }

    public void SetChangeSpriteIndex(ChangeSprite changeableSprite)
    {
        var imageIndexState = gameobjectIndexState.Find(x => x.name == changeableSprite.name);

        if (imageIndexState == null)
            return;

        changeableSprite.ChangeSpriteByIndex(imageIndexState.index);
    }

    public void UpdateImageState(ChangeSprite changeableSprite)
    {
        var imageIndexState = gameobjectIndexState.Find(x => x.name == changeableSprite.name);

        if (imageIndexState == null)
            return;

        imageIndexState.index = changeableSprite.SpriteIndex;
    }

    public class ImageIndexState
    {
        public string name;
        public int index;
    }
}


