﻿using UnityEngine;
using TMPro;

public class PlayFieldUIManager : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI[] _textCountdownTimers = null;
    [SerializeField]
    TextMeshProUGUI[] _textMessages = null;
    [SerializeField]
    Material _defaultFontMaterial = null;

    Material _currentFontMaterial = null;

    void OnEnable()
    {
        if (!GameManager.Instance)
            return;

        GameManager.Instance.CountdownTimer.OnTickEvent += UpdateTimers;
        GameManager.Instance.OnCountdownStart += EnableCountdownTextComponents;
        GameManager.Instance.OnGameStateChange += UpdateUI;
    }

    void OnDisable()
    {
        if (!GameManager.Instance)
            return;

        GameManager.Instance.CountdownTimer.OnTickEvent -= UpdateTimers;
        GameManager.Instance.OnCountdownStart -= EnableCountdownTextComponents;
        GameManager.Instance.OnGameStateChange -= UpdateUI;
    }

    void EnableCountdownTextComponents()
    {
        EnableTimers(true);
        EnableMessages(false);
    }

    void UpdateUI(GameInfo gameInfo)
    {
        if (gameInfo.GameState == GameState.Playing)
            EnableTimers(false);

        if(gameInfo.GameState == GameState.EndofRound)
        {
            EnableMessages(true);
            var winner = gameInfo.Winner;
            var message = "Draw!";
            _currentFontMaterial = _defaultFontMaterial;

            if (winner != null)
            {
                _currentFontMaterial = winner?.FontMaterial;
                message = gameInfo.Winner.Wins >= 3 ? $"{gameInfo.Winner.Name} won the game!" : $"{gameInfo.Winner.Name} wins!";
            }
            
            SetMessage(message);
        }

        if(gameInfo.GameState == GameState.GameOver)
        {
            _currentFontMaterial = _defaultFontMaterial;
            SetMessage("Game Over!");
        }
    }
    void EnableTimers(bool state)
    {
        foreach (var item in _textCountdownTimers)
            item.enabled = state;
    }

    void UpdateTimers(CountdownTimer countdownTimer)
    {
        string message = countdownTimer.Timer.ToString("0");

        foreach (var item in _textCountdownTimers)
            item.text = message;
    }

    void EnableMessages(bool state)
    {
        foreach (var item in _textMessages)
            item.enabled = state;
    }

    void SetMessage(string message)
    {
        foreach (var item in _textMessages)
        {
            item.fontMaterial = _currentFontMaterial;
            item.text = message;
        }
            
    }
}
