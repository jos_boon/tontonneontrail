﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public event Action<GameInfo> OnGameStateChange;
    public event Action<Player> OnPlayerJoined;
    public event Action OnCountdownStart;

    [SerializeField]
    CountdownTimer _countdownTimer = null;

    [Header("Game settings")]
    [SerializeField]
    bool _enablePowerUps = true;

    [Header("Countdown timers")]
    [SerializeField]
    float _timerPlayerSelect = 10.0f;
    [SerializeField]
    float _timerInGame = 3.0f;
    [SerializeField]
    float _delayBetweenRounds = 5.0f;

    GameState _gameState = GameState.None;
    Player _winner = null;
    List<Player> _players = null;
    Coroutine _runningCoroutine = null;

    public CountdownTimer CountdownTimer => _countdownTimer;

    public GameState GameState
    {
        get => _gameState;
        set
        {
            if (_gameState == value)
                return;

            _gameState = value;
            OnGameStateChange?.Invoke(new GameInfo(_winner, _gameState, _players.ToArray()));
        }
    }

    void Awake()
    {
        if (!SetInstance(this))
            return;

        _countdownTimer = GetComponent<CountdownTimer>();
        _players = new List<Player>(4);
        SceneManager.sceneLoaded += OnSceneLoaded;

        DontDestroyOnLoad(this);
    }

    void OnSceneLoaded(UnityEngine.SceneManagement.Scene scene, LoadSceneMode mode)
    {
        _runningCoroutine = null;

        if (scene.buildIndex == 0)
            InitializePlayerSelectScene();
        else if (scene.buildIndex == 1)
            InitializePlayScene();
    }

    IEnumerator LoadLevel(int index, float delay = 0.0f)
    {
        if (delay > 0)
            yield return new WaitForSeconds(delay);

        _countdownTimer.ClearEvents();

        SceneManager.LoadScene(index);
    }

    void InitializePlayerSelectScene()
    {
        _countdownTimer.Duration = _timerPlayerSelect;
        _countdownTimer.OnCompleteEvent += () => StartCoroutine(LoadLevel(1));
        _players.Clear();
        GameState = GameState.WaitingForPlayers;
    }

    void InitializePlayScene()
    {
        _countdownTimer.Duration = _timerInGame;
        _countdownTimer.OnCompleteEvent += PlayGame;

        if(PowerUpManager.Instance)
            PowerUpManager.Instance.enabled = _enablePowerUps;

        StartRound();
    }

    public void JoinPlayer(Player player)
    {
        if (player == null || GameState != GameState.WaitingForPlayers)
            return;

        _players.Add(player);
        OnPlayerJoined?.Invoke(player);

        if (_players.Count <= 1)
            return;

        StartCountdown(!_countdownTimer.IsRunning);
    }

    void StartCountdown(bool reset)
    {
        _countdownTimer.StartTimer(reset);
        OnCountdownStart?.Invoke();
    }

    public void PlayerLost(Player player)
    {
        if (player == null || GameState == GameState.EndofRound || GameState == GameState.GameOver)
            return;

        player.IsActive = false;

        //int activeCount = _players.Where(x => x.IsActive).Count();

        //if (activeCount <= 1)
        //    EndOfRound();

        if (_runningCoroutine == null)
            _runningCoroutine = StartCoroutine(DrawChecker());
    }

    IEnumerator DrawChecker()
    {
        yield return null;

        var activeCount = _players.Where(x => x.IsActive).Count();
        if (activeCount <= 1)
            EndOfRound();

        _runningCoroutine = null;
    }

    void StartRound()
    {
        foreach (var player in _players)
            player.IsActive = true;

        TrailRendererWith2DCollider.CleanUpTrails();
        GameState = GameState.StartRound;

        StartCountdown(true);
    }

    void PlayGame()
    {
        GameState = GameState.Playing;
    }

    void EndOfRound()
    {
        _winner = null;
        bool gameOver = false;
        // Only one player should be able to win
        _winner = _players.SingleOrDefault(x => x.IsActive);

        if (_winner != null)
        {
            _winner.Wins++;
            gameOver = _winner.Wins >= 3;
        }

        if(!gameOver && !IsInvoking("StartRound"))
            Invoke("StartRound", _delayBetweenRounds);
        else if (!IsInvoking("GameOver"))
            Invoke("GameOver", _delayBetweenRounds);

        GameState = GameState.EndofRound;
    }

    void GameOver()
    {
        GameState = GameState.GameOver;

        if(_runningCoroutine == null)
            _runningCoroutine = StartCoroutine(LoadLevel(0, _delayBetweenRounds));
    }
}

public class GameInfo
{
    public Player Winner { get; private set; }
    public GameState GameState { get; private set; }
    public Player[] JoinedPlayers { get; private set; }

    internal GameInfo(Player winner, GameState gameState, Player[] players)
    {
        Winner = winner;
        GameState = gameState;
        JoinedPlayers = players;
    }
}

public enum GameState
{
    WaitingForPlayers,
    StartRound,
    Playing,
    Pause,
    EndofRound,
    GameOver,
    None
}
