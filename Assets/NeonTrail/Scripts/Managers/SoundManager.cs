﻿using System;
using UnityEngine;

// Based on: https://www.youtube.com/watch?v=6OT43pvUyfY - Brackeys AudioManager
public class SoundManager : Singleton<SoundManager>
{
    [SerializeField]
    Sound[] sounds = null;
    
    void Awake()
    {
        if (!SetInstance(this))
            return;

        DontDestroyOnLoad(this);

        foreach (Sound sound in sounds)
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.volume = sound.volume;
            audioSource.pitch = sound.pitch;
            audioSource.clip = sound.audioClip;
            sound.audioSource = audioSource;
        }
    }

    public void Play(string soundName)
    {
        Sound s = Array.Find<Sound>(sounds, x => x.name == soundName);
        if(s == null)
        {
            Debug.Log($"SoundManager: {soundName} not found!");
            return;
        }

        s.audioSource.Play();
    }

    [Serializable]
    public class Sound
    {
        public string name;
        public AudioClip audioClip;
        [Range(0.0f, 1.0f)]
        public float volume = 1.0f;
        [Range(0.1f, 3.0f)]
        public float pitch = 1.0f;

        [HideInInspector]
        public AudioSource audioSource;
    }
}
