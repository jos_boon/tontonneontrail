﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ShipControl : ControllableObject
{
    [Header("Ship")]
    [SerializeField]
    float _speed = 100.0f;
    [SerializeField]
    float _turnSpeed = 80.0f;

    [Header("Animations")]
    [SerializeField]
    SpriteAnimator jumpUp = null;
    [SerializeField]
    SpriteAnimator fallDown = null;
    [SerializeField]
    SpriteAnimator flameBoost = null;

    [Header("Settings")]
    public Color shipColor = Color.white;
    public Color trailColor = Color.white;
    [HideInInspector]
    public List<PowerUp> activePowerUps;
    [SerializeField]
    PowerUp shipPowerUp;
    [SerializeField]
    bool flyForwardOnStart = true;

    [Header("Sound")]
    [SerializeField]
    string _explosionSFX = "Ship Destroyed";

    bool allowForwardMovement = true;

    Rigidbody2D shipBody;
    SpriteRenderer spriteRenderer;
    TrailRendererWith2DCollider trailRenderer;
    TrailGapBehaviour trailGapBehaviour;
    Collider2D[] colliders;
    Player _player;

    public float Speed
    {
        get => _speed;
        set => _speed = value;
    }
    public float TurnSpeed
    {
        get => _turnSpeed;
        set => _turnSpeed = value;
    }
    public Collider2D[] Colliders => colliders;
    public SpriteAnimator JumpUpAnimation => jumpUp;
    public SpriteAnimator FallDownAnimation => fallDown;
    public SpriteAnimator boostAnimation => flameBoost;

    public bool AllowForwardMovement
    {
        get
        {
            return allowForwardMovement;
        }
        set
        {
            allowForwardMovement = value;

            // Preventing unnessary gameobjects creations
            if (trailRenderer.Pausing != !value)
                trailRenderer.Pausing = !value;

            trailGapBehaviour.enabled = allowForwardMovement;
        }
    }

    public bool IsEmittingTrail
    {
        get
        {
            return !trailRenderer.Pausing;
        }
        set
        {
            // Trail can only be enabled when forward movement is happening
            if (!allowForwardMovement)
                return;

            // Preventing unnessary gameobjects creations
            if (trailRenderer.Pausing != !value)
                trailRenderer.Pausing = !value;

            trailGapBehaviour.enabled = value;

            if (trailGapBehaviour.enabled)
                trailGapBehaviour.SetPreviousPosition();
        }
    }

    void Awake()
    {
        shipBody = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        trailRenderer = GetComponentInChildren<TrailRendererWith2DCollider>();
        trailGapBehaviour = GetComponentInChildren<TrailGapBehaviour>();
        activePowerUps = new List<PowerUp>();
        colliders = GetComponentsInChildren<Collider2D>();
        
        if (!flyForwardOnStart)
            AllowForwardMovement = false;
    }

    void Start()
    {
        if(Application.isPlaying)
        {
            if(trailRenderer)
                trailRenderer.trailMaterial.color = trailColor;
        }
            
        spriteRenderer.color = shipColor;
    }

    void Update()
    {
        if(!Application.isPlaying)
            spriteRenderer.color = shipColor;
    }

    public void ChangeAbility(PowerUp powerUp)
    {
        if (shipPowerUp)
            Destroy(shipPowerUp.gameObject);

        shipPowerUp = powerUp;
    }

    public override void UseAbility()
    {
        if (!shipPowerUp)
            return;

        shipPowerUp.Apply(this);
    }

    void FixedUpdate()
    {
        if(allowForwardMovement)
            MoveForward();
    }

    void MoveForward()
    {
        shipBody.MovePosition(shipBody.position + (Vector2)transform.up * Speed * Time.deltaTime);
    }

    public override void Rotate(float direction)
    {
        shipBody.MoveRotation(shipBody.rotation + TurnSpeed * direction * Time.deltaTime);
    }

    public void DestroyShip()
    {
        // Only trigger once
        if (!gameObject.activeSelf)
            return;

        GameManager.Instance?.PlayerLost(_player);
        SoundManager.Instance?.Play(_explosionSFX);

        gameObject.SetActive(false);
    }
    
    public void AttachPlayer(Player player) => _player = player;
}
