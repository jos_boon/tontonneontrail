﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DestroyShipOnHit : MonoBehaviour
{
    ShipControl ship;

    void Awake()
    {
        ship = GetComponentInParent<ShipControl>();

        if(!ship)
        {
            Debug.LogWarning("DestroyShipOnHit: No ShipControl script found in parent!");
            enabled = false;
        }
            
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        ship.DestroyShip();
    }
}
