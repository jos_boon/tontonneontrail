﻿using UnityEngine;

public abstract class ControllableObject : MonoBehaviour
{
    CrossInput _inputDevice;

    public CrossInput InputDevice 
    { 
        get => _inputDevice;
        set
        {
            if (InputDevice == value)
                return;

            var previousInputDevice = InputDevice;
            _inputDevice = value;

            if (InputDevice && InputDevice.ControllableObject != this)
                InputDevice.ControllableObject = this;

            if (previousInputDevice && previousInputDevice.ControllableObject == this)
                previousInputDevice.ControllableObject = null;
        }
    }

    public abstract void Rotate(float direction);
    public abstract void UseAbility();
}
