﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArduinoInput : CrossInput
{
    public float directionAmplifier = 2.0f;

    protected override void Start()
    {
        base.Start();

        if (!ArdityInputReader.Instance)
        {
            Debug.Log($"ArduinoInput: No ArdityInputReader found!! Disabling Object: {gameObject.name}");
            gameObject.SetActive(false);
        }

    }

    protected override void RetrieveInput()
    {
        _direction = 0;
        _useAbility = false;

        var playerData = ArdityInputReader.Instance[_playerIndex];

        if (playerData == null || playerData.consumed)
            return;

        _direction = playerData.direction * directionAmplifier;
        _useAbility = playerData.buttonPress;

        playerData.consumed = true;
    }
}
