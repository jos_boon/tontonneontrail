﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CrossInput : MonoBehaviour
{
    [SerializeField]
    protected int _playerIndex = -1; // -1 No Player
    [Tooltip("Execute GetComponent for IInputControlled in Start call")]
    [SerializeField]
    bool _connectInputComponent = false;
    [SerializeField]
    ControllableObject _controllableObject;

    protected float _direction;
    protected bool _useAbility;

    public ControllableObject ControllableObject
    {
        get => _controllableObject;
        set
        {
            if (ControllableObject == value)
                return;

            var previousControllableObject = ControllableObject;
            _controllableObject = value;

            if (ControllableObject && ControllableObject.InputDevice != this)
                ControllableObject.InputDevice = this;

            if (previousControllableObject && previousControllableObject.InputDevice == this)
                previousControllableObject.InputDevice = null;
        }
    }

    protected virtual void Start()
    {
        if (_controllableObject)
            _controllableObject.InputDevice = this;
        else if(_connectInputComponent)
            ControllableObject = GetComponent<ControllableObject>();
    }

    void Update()
    {
        if (ControllableObject == null)
            return;

        RetrieveInput();

        if (_useAbility)
            ControllableObject.UseAbility();
    }

    void FixedUpdate()
    {
        if (ControllableObject == null)
            return;

        if (_direction == 0)
            return;

        ControllableObject.Rotate(_direction);
        _direction = 0;
    }

    protected abstract void RetrieveInput();
}

