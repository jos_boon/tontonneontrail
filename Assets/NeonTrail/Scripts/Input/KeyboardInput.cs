﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardInput : CrossInput
{
    public KeyCode keyLeft;
    public KeyCode keyRight;
    public KeyCode keyAbility;

    protected override void RetrieveInput()
    {
        _direction = 0;
        _useAbility = false;
        
        if (Input.GetKey(keyLeft))
            _direction = 1; 
        else if (Input.GetKey(keyRight))
            _direction = -1;

        if (Input.GetKeyDown(keyAbility))
            _useAbility = true;            
    }
}
