﻿using UnityEngine;

[RequireComponent(typeof(CrossInput))]
public class PlayerDetector : ControllableObject
{
    [SerializeField]
    bool _detectOnRotation = true;
    [SerializeField]
    bool _detectOnAbilityUse = false;
    [SerializeField]
    Player _player = null;

    public string PlayerName => _player?.Name ?? "UNKNOWN";

    void OnEnable()
    {
        if (GameManager.Instance)
            GameManager.Instance.OnGameStateChange += Reset;
    }

    void OnDisable()
    {
        if (GameManager.Instance)
            GameManager.Instance.OnGameStateChange -= Reset;
    }

    public override void Rotate(float direction)
    {
        if (!_detectOnRotation)
            return;

        RegisterPlayer();
    }

    public override void UseAbility()
    {
        if (!_detectOnAbilityUse)
            return;

        RegisterPlayer();
    }

    void RegisterPlayer()
    {
        _player.InputDevice = InputDevice;
        InputDevice = null;
        GameManager.Instance?.JoinPlayer(_player);
    }

    void Reset(GameInfo gameInfo)
    {
        if (gameInfo.GameState != GameState.WaitingForPlayers)
            return;

        _player.Reset();
        InputDevice = GetComponent<CrossInput>();
    }
}
