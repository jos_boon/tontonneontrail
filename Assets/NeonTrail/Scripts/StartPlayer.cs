﻿using UnityEngine;

[RequireComponent(typeof(Spawner))]
public class StartPlayer : MonoBehaviour
{
    [SerializeField]
    string _name = "Player Name";

    Spawner _spawner;
    ShipControl _ship;

    void Awake()
    {
        _spawner = GetComponent<Spawner>();
    }

    void OnEnable()
    {
        if (GameManager.Instance)
            GameManager.Instance.OnGameStateChange += RoundStart;
    }

    void OnDisable()
    {
        if (GameManager.Instance)
            GameManager.Instance.OnGameStateChange -= RoundStart;
    }

    void RoundStart(GameInfo gameInfo)
    {
        Player player = null;
        foreach(var p in gameInfo.JoinedPlayers)
            player = p.Name == _name ? p : player;

        if (player == null)
            return;

        if(gameInfo.GameState == GameState.StartRound)
        {
            // Spawn ship if it doesn't exist and attach the player to it
            if(!_ship)
            {
                //  Start Player had been destroyed..
                _ship = _spawner.SpawnObject(nameOfObject: _name, parent: transform)?.GetComponent<ShipControl>();
                // Null checking
                if (!_ship)
                    return;

                _ship.InputDevice = player.InputDevice;
                _ship.AttachPlayer(player);
            }
            
            // Prepare ship state for when the game starts
            if (_ship)
            {
                _ship.transform.localPosition = Vector3.zero;
                _ship.transform.localRotation = Quaternion.identity;
                _ship.gameObject.SetActive(true);
                _ship.AllowForwardMovement = false;
            }
        }
        else if (gameInfo.GameState == GameState.Playing)
            _ship.AllowForwardMovement = true;
    }

}
