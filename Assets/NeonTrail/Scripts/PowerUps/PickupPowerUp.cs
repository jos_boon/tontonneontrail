﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(CircleCollider2D))]
public class PickupPowerUp : MonoBehaviour
{
    [SerializeField]
    PowerUp[] powerUps = null;
    [SerializeField]
    bool activateOnPickUp = false;
    [SerializeField]
    bool replacePlayerAbility = false;

    [Header("Sound")]
    [SerializeField]
    string soundName = "Powerup Pickup";

    bool pickedUp = false;

    void Start()
    {
        // Destroy if no power ups present
        if (powerUps.Length == 0)
        {
            Destroy(gameObject);
            return;
        }  
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.CompareTag("PlayerPickup") || pickedUp)
            return;

        pickedUp = true;

        ShipControl ship = collision.GetComponentInParent<ShipControl>();

        for(int i = 0; i < powerUps.Length; i++)
        {
            if(powerUps[i])
                powerUps[i] = Instantiate(powerUps[i], ship.transform);
        }
            
        if (activateOnPickUp)
        {
            foreach (PowerUp powerup in powerUps)
            {
                if(powerup)
                    powerup.Apply(ship);
            }
        }

        //if (replacePlayerAbility && powerUps[0])
        //    ship.ChangeAbility(powerUps[0]);


        SoundManager.Instance?.Play(soundName);
        //  Remove pickup
        Destroy(gameObject);
    }
}
