﻿using UnityEngine;
using System.Collections;

// TODO: Add option for power ups to stack or not
// TODO: Add option so Power ups are either additive, subtractive or absolute in changing values (stacking or no stacking)
// TODO: Power ups need to properly reset the ship values back to their original values;
public abstract class PowerUp : MonoBehaviour
{
    [SerializeField]
    protected bool affectSelf;
    [SerializeField]
    protected bool affectOthers;
    [SerializeField]
    CountdownTimer powerUpDuration = null;
    [SerializeField]
    bool destroyOnUndo = false;
    [SerializeField]
    float cooldownTimer = 2.0f;

    protected ShipControl affectedPlayerShip;
    protected ShipControl otherShips;

    [SerializeField]
    bool onCooldown = false;

    IEnumerator cooldown;

    protected abstract void AddPowerUp(ShipControl ship);
    protected abstract void RemovePowerUp(ShipControl ship);

    protected virtual void Awake()
    {
        if (powerUpDuration)
            powerUpDuration.OnCompleteEvent += Undo;
    }

    protected virtual void OnDisable()
    {
        if(cooldown != null)
            StopCoroutine(cooldown);

        onCooldown = false;

        if (powerUpDuration)
            powerUpDuration.StopTimer();

        Undo();
    }

    IEnumerator CooldownPowerUp()
    {
        yield return new WaitForSeconds(cooldownTimer);

        onCooldown = false;
    }

    //public void ReplacePowerUp()
    //{
    //    StopCoroutine(DelayRemovePowerUp());
    //    Destroy(this);
    //}
    
    public PowerUp DuplicatePowerUp(ShipControl ship)
    {
        foreach (PowerUp powerUp in ship.activePowerUps)
        {
            if (powerUp.GetType() == this.GetType())
                return powerUp;      
        }

        return null;
    }

    public virtual void Apply(ShipControl ship)
    {
        if (onCooldown || !gameObject.activeSelf || !ship.gameObject.activeSelf)
            return;

        onCooldown = true;

        affectedPlayerShip = ship;

        if (affectSelf)
            AddPowerUp(affectedPlayerShip);

        if(affectOthers)
        {
            if(!PowerUpManager.Instance)
            {
                Debug.Log("PowerUp: PowerUp Manager missing! Cannot affect other ships");
                return;
            }

            foreach(ShipControl otherShip in PowerUpManager.Instance.AllShips)
            {
                if (ship == otherShip || !otherShip)
                    continue;

                otherShip.activePowerUps.Add(this);
                AddPowerUp(otherShip);
            }
        }

        if (powerUpDuration)
            powerUpDuration.StartTimer();

        cooldown = CooldownPowerUp();
        StartCoroutine(cooldown);
    }

    public virtual void Undo()
    {
        if(affectedPlayerShip)
        {
            affectedPlayerShip.activePowerUps.Remove(this);
            if (affectSelf)
                RemovePowerUp(affectedPlayerShip);
        }

        if(affectOthers)
        {
            foreach(ShipControl otherShip in PowerUpManager.Instance.AllShips)
            {
                if (affectedPlayerShip == otherShip || !otherShip)
                    continue;

                otherShip.activePowerUps.Remove(this);
                RemovePowerUp(otherShip);
            }
        }

        if(destroyOnUndo)
            Destroy(gameObject);

        if (powerUpDuration)
            powerUpDuration.ResetTimer();
    }
}
