﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScalePowerUp : PowerUp
{
    public Vector3 additiveScale = new Vector3(.2f, .2f, 0);

    protected override void AddPowerUp(ShipControl ship)
    {
        //PowerUp doublePowerUp = DuplicatePowerUp(affectedPlayerShip);

        //if(!doublePowerUp)
        //    affectedPlayerShip.transform.localScale += additiveScale;
        //else
        //{
        //    doublePowerUp.ReplacePowerUp();
        //    affectedPlayerShip.activePowerUps.Remove(doublePowerUp);
        //}

        ship.transform.localScale += additiveScale;
    }

    protected override void RemovePowerUp(ShipControl ship)
    {
        if(ship)
            ship.transform.localScale -= additiveScale;
    }
}
