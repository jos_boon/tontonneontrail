﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPowerUp : PowerUp
{
    public int speedBoost = 2;

    protected override void AddPowerUp(ShipControl ship)
    {
        //PowerUp doublePowerUp = DuplicatePowerUp(affectedPlayerShip);

        //if(!doublePowerUp)
        //    affectedPlayerShip.speed += speedBoost;
        //else
        //{
        //    doublePowerUp.ReplacePowerUp();
        //    affectedPlayerShip.activePowerUps.Remove(doublePowerUp);
        //}

        //affectedPlayerShip.activePowerUps.Add(this);

        if (ship.boostAnimation)
            ship.boostAnimation?.Play();

        ship.Speed += speedBoost;
    }

    protected override void RemovePowerUp(ShipControl ship)
    {
        if (!ship)
            return;

        ship.Speed -= speedBoost;

        if(ship.boostAnimation)
            ship.boostAnimation.Stop(true);
    }
}
