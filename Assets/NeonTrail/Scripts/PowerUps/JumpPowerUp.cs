﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPowerUp : PowerUp
{
    [SerializeField]
    float airTime = .3f;

    [Header("Sound")]
    [SerializeField]
    string soundName = "Jump";

    protected override void OnDisable()
    {
        base.OnDisable();

        StopAllCoroutines();
    }

    protected override void AddPowerUp(ShipControl ship)
    {
        foreach (Collider2D col in ship.Colliders)
            col.enabled = false;

        ship.IsEmittingTrail = false; 
        ship.JumpUpAnimation?.Play();

        SoundManager.Instance?.Play(soundName);

        StartCoroutine(Delay(ship));
    }

    protected override void RemovePowerUp(ShipControl ship)
    {
        if (!ship || !ship.FallDownAnimation)
            return;

        // TODO: We are adding the same funtion each time we jump
        ship.FallDownAnimation.OnAnimationFinishedEvent += () => ReEnableColliders(ship);
        ship.FallDownAnimation?.Play();
    }

    void ReEnableColliders(ShipControl ship)
    {
        if (!ship)
            return;

        ship.IsEmittingTrail = true;

        foreach (Collider2D col in ship.Colliders)
            col.enabled = true;
    }

    IEnumerator Delay(ShipControl ship)
    {
        yield return new WaitForSeconds(airTime);

        RemovePowerUp(ship);
    }
}
