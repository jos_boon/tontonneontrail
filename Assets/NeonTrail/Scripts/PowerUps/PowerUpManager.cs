﻿using System.Collections;
using UnityEngine;

public class PowerUpManager : Singleton<PowerUpManager>
{
    [SerializeField]
    PickupPowerUp[] PowerUps = null;
    [SerializeField]
    float minTimer = 10;
    [SerializeField]
    float maxTimer = 15;
    [Tooltip("Bounds where the power ups will spawn. Center Position is locked onto the transform position")]
    [SerializeField]
    Bounds _bounds = new Bounds();

    Transform _parentPowerUp = null;
    IEnumerator _powerUpCoroutine = null;
    
    public ShipControl[] AllShips { get; private set; }

    void Awake()
    {
        if (!SetInstance(this))
            return;

        AllShips = GameObject.FindObjectsOfType<ShipControl>();
        _parentPowerUp = new GameObject("PowerUps").transform;
        _powerUpCoroutine = SpawnPowerUp();
    }

    void OnEnable()
    {
        if (_powerUpCoroutine != null)
            StartCoroutine(_powerUpCoroutine);
        else
            Debug.LogWarning("PowerUpManager: Failed to start SpawnPowerUp! Coroutine is null!");
    }

    void OnDisable()
    {
        if (_powerUpCoroutine != null)
            StopCoroutine(_powerUpCoroutine);
    }

    void OnDrawGizmosSelected()
    {
        _bounds.center = transform.position;
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(_bounds.center, _bounds.size);
    }

    IEnumerator SpawnPowerUp()
    {
        while (PowerUps.Length > 0)
        {
            yield return new WaitForSeconds(Random.Range(minTimer, maxTimer));

            PickupPowerUp powerUp = PowerUps[Random.Range(0, PowerUps.Length)];
            var x = Random.Range(_bounds.min.x, _bounds.max.x);
            var y = Random.Range(_bounds.min.y, _bounds.max.y);
            var z = 0.0f;
            var spawnPosition = new Vector3(x, y, z);

            // TODO: Check if power Up position is free of obstacles
            Instantiate(powerUp.gameObject, spawnPosition, Quaternion.identity, _parentPowerUp);
        }
    }
}
