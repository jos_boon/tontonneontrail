﻿using UnityEngine;

public class AwardStar : MonoBehaviour
{
    [SerializeField]
    PlayerStarLocation[] _playerStarLocations = null;

    void OnEnable()
    {
        if (GameManager.Instance)
            GameManager.Instance.OnGameStateChange += RewardStarToWinner;
    }

    void OnDisable()
    {
        if (GameManager.Instance)
            GameManager.Instance.OnGameStateChange -= RewardStarToWinner;
    }

    void RewardStarToWinner(GameInfo gameInfo)
    {
        if (gameInfo.Winner == null || gameInfo.GameState != GameState.EndofRound)
            return;

        Player winner = gameInfo.Winner;

        foreach (var playerStarLocation in _playerStarLocations)
        {
            if (playerStarLocation.Name != winner.Name)
                continue;

            var parent = playerStarLocation.TargetLocations[winner.Wins - 1];
            Transform star = playerStarLocation.SpawnFrom.SpawnObject(winner.Name, transform.position, Quaternion.identity, parent);
            star?.GetComponent<MoveToTarget>()?.SetTarget(parent.position);
        }
    }

    [System.Serializable]
    public struct PlayerStarLocation
    {
        [SerializeField]
        string _name;
        [SerializeField]
        Spawner _spawnFrom;
        [SerializeField]
        Transform[] _targetLocations;

        public PlayerStarLocation(string name, Spawner spawner, params Transform[] targetLocations)
        {
            _name = name;
            _spawnFrom = spawner;
            _targetLocations = targetLocations;
        }

        public string Name => _name;
        public Spawner SpawnFrom => _spawnFrom;
        public Transform[] TargetLocations => _targetLocations;
    }
}
