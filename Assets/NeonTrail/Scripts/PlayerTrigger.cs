﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ToggleGameobjects))]
public class PlayerTrigger : MonoBehaviour
{
    [SerializeField]
    Player.PlayerCorner _mappedCorner = Player.PlayerCorner.None;

    ControllableObject _controllableObject = null;
    ToggleGameobjects _toggler;
    
    void Start()
    {
        _toggler = GetComponent<ToggleGameobjects>();
        _controllableObject = GetComponentInChildren<ControllableObject>(true);
    }

    void OnEnable()
    {
        if (GameManager.Instance)
            GameManager.Instance.OnPlayerJoined += NewPlayer;
    }

    void OnDisable()
    {
        if (GameManager.Instance)
            GameManager.Instance.OnPlayerJoined -= NewPlayer;
    }

    void NewPlayer(Player player)
    {
        if (player.playerCorner != _mappedCorner)
            return;

        player.InputDevice.ControllableObject = _controllableObject;
        _toggler.Toggle();
    }
}
