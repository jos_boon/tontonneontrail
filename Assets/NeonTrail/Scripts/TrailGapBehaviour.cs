﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TrailRendererWith2DCollider))]
public class TrailGapBehaviour : MonoBehaviour
{
    [SerializeField]
    float trailDistance = 10.0f;
    [SerializeField]
    float gapDistance = 3.0f;

    TrailRendererWith2DCollider trail;
    float distanceTraveled;
    Vector3 previousPosition;

    void Start()
    {
        trail = GetComponent<TrailRendererWith2DCollider>();
        previousPosition = transform.position;
    }

    public void SetPreviousPosition()
    {
        previousPosition = transform.position;
        distanceTraveled = 0;
    }

    void LateUpdate()
    {
        distanceTraveled += (previousPosition - transform.position).magnitude;

        if (trail.Pausing)
            SetPauseState(gapDistance, false);
        else
            SetPauseState(trailDistance, true);

        previousPosition = transform.position;

        void SetPauseState(float distance, bool setStateIfTrue)
        {
            if (distanceTraveled >= distance)
            {
                trail.Pausing = setStateIfTrue;
                distanceTraveled = 0;
            }   
        }   
    }
}
