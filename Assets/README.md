# TonTon Neon Trail Unity Project
![Readme Header](Images/readme_header.png)

## Contents
1. [Project structure](#project-structure)
2. [Arduino and the rotary encoder](#arduino-and-the-rotary-encoder)
3. [Neon Trail setup](#neon-trail-setup)
4. [Howto play](#howto-play)
5. [Kinect](#kinect)
6. [Future changes](#future-changes)
7. [packages and sources](#packages-and-sources)

## 1. Project structure
First level view of the Asset folder structure. Most folders within these folders are self explanitory.

* **Ardity:** Contains script, prefabs and example scenes for commmunicating with the arduino
* **KinectAsset:** Contains the SDK for Kinect 2.0
* **KinectView:** Contains scripts for communicating with Kinect; also has an example scene
* **NeonTrail:** The actual game
* **Plugins:** Required for Kinect SDK to be used within the Unity scripting environment
* **TextMesh Pro:** Unity's text component

## 2. Arduino and the rotary encoder
To play the game with an Arduino you will need the following components.

- 1x Arduino Uno
- 4x Rotary encoders
- Bunch of wires (how many depends on the Rotary encoder)
- Bunch of resistors (how many and the strength of depends on the Rotary encoder)

For a list of Rotary encoders [click here][2].

We have been using the Rotary encoder EC12D, it needs minimal 5k ohm Resistors and its technical page can be found [here][1].
At the bottom of the third page (numbered as 278) you will find how to connect the encoder.

If you are still in possesion of the prototype dials that we built you will be able connect them to the Arduino.

Order of connecting them:
1. Purple (pink box) - Player One
2. Red - Player Two
3. Blue - Player Three
4. Green - Player Four

The Red wire needs to be connected to the 5 Volt of the arduino.
Black wire with blue sticker needs to be connected to the Ground of the arduino.
This leaves 3 wires left, 2 unmarked wires, which are input A & B (And yes the should have been marked as well :), and one marked with an orange sticker, the button also known as input C.
These wires need to be connected in the arduino digital pins starting from pin number 2.
First you will connect the 2 unmarked wires and after that the orange marked wire that will be the seperator between dials.

Example:
Pin 2 has input A of player One
Pin 3 has input B of player One
Pin 4 has input C of player One
.. Continue with this pattern

If confused check the rotary script found in NeonTrail/ArduinoScript/RotaryEncoder. At the top of the script you will see which pins are defined and assigned to.
If the dial input is flipped then you will need to swap the A and B wires.

## 3. Neon Trail Setup
The game can be played with the keyboard or with the custom Dials using Arduino.

In the Prefabs/Managers folder of Neon Trail you can find 2 GameManagers:

Prefab GameManager is setup for playing with the keyboard.
Prefab GameManager - Arduino is setup for playing with the Arduino.

Also note that the GameManager script in general is setup to only work properly when the game starts in PlayerSelect Scene.
The GameManager can be added to other scenes but it need to be manually setup to work properly.

If you want to fly a ship within a new scene you will need to add the following prefabs:
- NeonTrail/Prefabs GameManager
- NeonTrail/Prefabs Ship
- Ardity/Prefabs SerialController (if using Arduino as input)

To the Ship gameobject you can now add the arduino or keyboard input component script and drag the ShipControl script into the empty field called ship.
In the GameManager you can disable various child gameobjects to empty the screen (like UI). In the inspector of GameManager you can empty the Players List as it is not needed in this scene.

For the SerialController you might want to up the Max Unread Messages variable to 20 (or higher) for better input reading.

If you want to simply play the Game as-is:

1. Open PlayerSelectScene
2. Enable the GameManager with the desired Input
3. Disable the GameManager with the undesired Input
4. *if arduino* Connect Arduino and the dials
5. Press Play

## 4. Howto play
To play the game simply open the PlayerSelect Scene and press play (assuming you followed the previous Chapter).

Rules:
 - Ship always moves forward
 - Crashing into other players, borders, trails left by the players or obstacles (red neon circles) destroys your ship
 - Input only allows left and right movement and the ability usages (currently Jump)
 - First to accuire 3 stars wins the game!

Power ups (if turned on in the GameManager):

There are 2 kinds of powers in the game. 

- Power ups that only affect the player that picks it up 
- power ups that only affects the other players

There is at the moment Scale up (others), Slow (self & others) and Speed (self & others) as power ups.

Controls (Keyboard):
The ships can turn left and right and use their ability by using the following keys.

Green:	Left Arrow (left), Right Arrow (right), Up Arrow (ability)
Purple: J (left), L (right), I (Ability)
Blue:	A (left), D (right), W (Ability)
Red:	F (left), H (right), T (Ability)

Controls (Arduino):

Players use a dial to go left or right and it can also be pressed to use their ability.

Other:

Tab:				Goto next background
Left mouse click:	Spawn an Obstacle gameobject within the field
Right mouse click:	Remove an gameobject with tag 'Obstacle'

## 5. Kinect
At this moment there is no Kinect integration within the game of Neon Trail. However the SDK with demo scene is still present in the project if one wants to work with it.

A good starting point would probally be [here][KINECT].

For the demo scene (located in KinectView folder) simply open it and press play. If the Kinect is properly connected it will immediately show some results.

## 6. Future Changes

This section is meant for the programmers and not much for the artist or designer, however some changes might still require changes or attention from these departments.

Reason for this section is because certain features or scripts have been rushed and are need of a rework. 

- Change the global settings of the ships to a Scriptable Object
- Create a Scriptable Object for the different ship settings (like the ship sprite) instead of having multiple prefabs of the same
- Decouple the GameManager and UI from each other
- Decouple the input detection from the GameManager
- Redo the UI
- PowerUps should make beter use of the countdown script
- Perhaps a Countdown Manager script that allows other scripts to start a countdown timer
- A more Customizable Countdown class that works with IEnumerator and allows the designer to set custom delays between each count
- Better communication between Input -> player -> UI (its kinda messy now)
- Shader scripts for easier customizations of sprites, backgrounds, ect.
- Check TODO's in scripts

## 7. Packages and Sources

Packages and some of the sources we used.

Packages:
- Kinect SDK for unity - [link][KINECT]
- Ardity - [link][ARDITY]
- TextMesh Pro - [link][TMP]

Sources:
- [TrailRendererWith2DCollider](http://wiki.unity3d.com/index.php/TrailRendererWith2DCollider)
- [Getting Started with Kinect v2, Unity 3D, and C#](http://www.voratima.com/getting-started-with-kinect-v2-unity-3d-and-c/)
- [How to integrate Arduino with Unity](https://www.alanzucconi.com/2015/10/07/how-to-integrate-arduino-with-unity/)

[1]: https://www.alps.com/prod/info/E/PDF/Encoder/Incremental/EC12E/EC12E.pdf
[2]: https://www.alps.com/prod/info/E/HTML/Encoder/Incremental/EC12E/EC12E_list.html
[ARDITY]: https://ardity.dwilches.com/
[TMP]: https://docs.unity3d.com/Packages/com.unity.textmeshpro@2.0/manual/index.html
[KINECT]: https://developer.microsoft.com/en-us/windows/kinect
